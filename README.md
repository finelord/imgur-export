# IMGUR export tool

This tool was created to manage IMGUR albums and create data files for another application (kind of traveler's photo album).

The application allows:

- oAuth to IMGUR (IMGUR app with client ID required)
- read and display a list of albums
- display photos from selected album
- group same-named photos with different size
- edit image titles
- copy a direct link to the image in one click
- export selected photos as a JSON picking up suitable photo sizes for 1x, 2x and previews

![imgur-manager.png](https://bitbucket.org/repo/9aM7rx/images/2454100010-imgur-manager.png)

## Start

```sh
$ npm install
$ npm start
```