/* eslint no-unused-expressions: 0 */
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ConnectStatus from 'components/ConnectStatus';

describe('ConnectStatus component', () => {

  it('should display connected profile name if connected successfuly', () => {
    const accountInfo = { url: 'TestProfileName' };
    const component = shallow(
      <ConnectStatus isConnected accountInfo={accountInfo} />
    );
    expect(component.find('span')).to.have.text().match(/^connected to/);
    expect(component.find('strong')).to.have.text().match(/^TestProfileName/);
  });

  it('should display "connected to undefined source" if account info is not available', () => {
    const component = shallow(
      <ConnectStatus isConnected accountInfo={{}} />
    );
    expect(component.find('span'))
      .to.have.text().match(/^connected to undefined source/);
  });

  it('should display "no connection" in case of no connection', () => {
    const component = shallow(
      <ConnectStatus isConnected={false} />
    );
    expect(component.find('span')).to.have.text().match(/^no connection/);
  });

});
