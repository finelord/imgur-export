import { expect } from 'chai';
import albums from 'reducers/albums';
import {
  REQUEST_ALBUMS,
  REQUEST_ALBUMS_SUCCESS,
  REQUEST_ALBUMS_FAIL
} from 'actions/albums';

const albumsList = [ {}, {}, {} ];
const albumsStateSuccess = {
  status: 'success',
  response: [ {}, {}, {} ],
}

export default function() {
  describe('albums2', () => {
    it('should handle initial state', () => {
      const state = albums(undefined, {});
      expect(state).to.deep.equal({});
    });

    it('should handle REQUEST_ALBUMS', () => {
      const state = albums({}, { type: REQUEST_ALBUMS });
      expect(state).to.deep.equal({ status: 'loading' });
    });

    it('should handle REQUEST_ALBUMS_FAIL', () => {
      const state = albums({}, { type: REQUEST_ALBUMS_FAIL });
      expect(state).to.deep.equal({ status: 'failed' });
    });

    it('should handle REQUEST_ALBUMS_SUCCESS', () => {
      const state = albums({}, { type: REQUEST_ALBUMS_SUCCESS, albumsList });
      expect(state).to.deep.equal(albumsStateSuccess);
    });

    it('should handle unknown action type', () => {
      const state = albums(albumsStateSuccess, { type: 'unknown' });
      expect(state).to.equal(albumsStateSuccess);
    });
  });
}
