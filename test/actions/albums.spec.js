/* eslint no-unused-expressions: 0 */
import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import nock from 'nock';
import thunk from 'redux-thunk';
import { push } from 'react-router-redux';
// import { spy } from 'sinon';
import * as actions from 'actions/albums';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const albumsList = [{}, {}, {}];

describe('actions', () => {
  describe('albums', () => {
    afterEach(() => {
      nock.cleanAll();
    });

    it('requestAlbums should create requestAlbums action', () => {
      expect(actions.requestAlbums())
        .to.deep.equal({ type: actions.REQUEST_ALBUMS });
    });

    it('requestAlbumsSuccess should create requestAlbumsSuccess action', () => {
      expect(actions.requestAlbumsSuccess(albumsList))
        .to.deep.equal({
          type: actions.REQUEST_ALBUMS_SUCCESS,
          albumsList,
        });
    });

    it('requestAlbumsFail should create requestAlbumsFail action', () => {
      expect(actions.requestAlbumsFail())
        .to.deep.equal({ type: actions.REQUEST_ALBUMS_FAIL });
    });

    it('fetchAlbums should create requestAlbumsSuccess on success', () => {
      nock('https://api.imgur.com/')
        .get('/3/account/me/albums')
        .reply(200, { success: true, status: 200, data: albumsList });

      const store = mockStore({ connect: { accessToken: 'fake_token' } });
      const expectedActions = [
        { type: actions.REQUEST_ALBUMS },
        { type: actions.REQUEST_ALBUMS_SUCCESS, albumsList },
      ];

      return store.dispatch(actions.fetchAlbums())
        .then(() => {
          expect(store.getActions()).to.deep.equal(expectedActions);
        });
    });

    it('fetchAlbums should create requestAlbumsFail and redirect to error page on error', () => {
      nock('https://api.imgur.com/')
        .get('/3/account/me/albums')
        .reply(500, { success: false, status: 500 });

      const store = mockStore({ connect: { accessToken: 'fake_token' } });
      const expectedActions = [
        { type: actions.REQUEST_ALBUMS },
        push('/error'),
        { type: actions.REQUEST_ALBUMS_FAIL },
      ];

      return store.dispatch(actions.fetchAlbums())
        .then(() => {
          expect(store.getActions()).to.deep.equal(expectedActions);
        });
    });

    // it('fetchAlbums should create...', () => {
    //   const fn = actions.fetchAlbums();
    //   expect(fn).to.be.a('function');
    //   const dispatch = spy();
    //   const getState = () => ({ connect: { accessToken: 'fake_token' } });
    //   fn(dispatch, getState);
    //   expect(dispatch).to.have.been.calledWith({ type: actions.REQUEST_ALBUMS });
    // });
  });
//
//   it('incrementIfOdd should create increment action', () => {
//     const fn = actions.incrementIfOdd();
//     expect(fn).to.be.a('function');
//     const dispatch = spy();
//     const getState = () => ({ counter: 1 });
//     fn(dispatch, getState);
//     expect(dispatch).to.have.been.calledWith({ type: actions.INCREMENT_COUNTER });
//   });
//
//   it('incrementIfOdd shouldnt create increment action if counter is even', () => {
//     const fn = actions.incrementIfOdd();
//     const dispatch = spy();
//     const getState = () => ({ counter: 2 });
//     fn(dispatch, getState);
//     expect(dispatch).to.have.not.been.called;
//   });
//
//   // There's no nice way to test this at the moment...
//   it('incrementAsync', (done) => {
//     const fn = actions.incrementAsync(1);
//     expect(fn).to.be.a('function');
//     const dispatch = spy();
//     fn(dispatch);
//     setTimeout(() => {
//       expect(dispatch).to.have.been.calledWith({ type: actions.INCREMENT_COUNTER });
//       done();
//     }, 5);
//   });
});
