import {
  REQUEST_ALBUMS,
  REQUEST_ALBUMS_SUCCESS,
  REQUEST_ALBUMS_FAIL,
} from 'actions/albums';

const initState = {
  response: [],
};

export default function albums(state = initState, action) {
  switch (action.type) {

    case REQUEST_ALBUMS:
      return {
        response: [],
        status: 'loading',
      };

    case REQUEST_ALBUMS_SUCCESS:
      return {
        response: action.albumsList,
        status: 'success',
      };

    case REQUEST_ALBUMS_FAIL:
      return {
        response: [],
        status: 'failed',
      };

    default:
      return state;

  }
}

export const getAlbums = (state) => state.response.sort((a, b) => a.order - b.order);
export const getAlbumsStatus = (state) => state.status;
