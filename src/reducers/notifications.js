import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
} from 'actions/notifications';

export default function notifications(state = {}, action) {
  switch (action.type) {

    case ADD_NOTIFICATION:
      return {
        ...state,
        [action.notificationId]: {
          message: action.message,
          messageType: action.messageType,
          notificationId: action.notificationId,
        },
      };

    case REMOVE_NOTIFICATION: {
      const notificationsObj = { ...state };
      delete notificationsObj[action.notificationId];
      return notificationsObj;
    }

    default:
      return state;

  }
}

export const getNoticications = (state) =>
  Object.keys(state)
    .sort()
    .map((key) => state[key]) || [];
