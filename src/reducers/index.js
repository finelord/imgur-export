import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import connect, * as fromAuth from './connect';
import album, * as fromAlbum from './album';
import albums, * as fromAlbums from './albums';
import notifications, * as fromNotifications from './notifications';

export default combineReducers({
  connect,
  album,
  albums,
  notifications,
  routing,
});

// connect selectors
export const isConnected = (state) => fromAuth.isConnected(state.connect);
export const getAccessToken = (state) => fromAuth.getAccessToken(state.connect);
export const getAccountInfo = (state) => fromAuth.getAccountInfo(state.connect);

// album selectors
export const getAlbum = (state) => fromAlbum.getAlbum(state.album);
export const getAlbumView = (state) => fromAlbum.getAlbumView(state.album);
export const getImages = (state) => fromAlbum.getImages(state.album);
export const getImageById = (state, id) => fromAlbum.getImageById(state.album, id);
export const getIsImageDeletion = state => fromAlbum.getIsImageDeletion(state.album);
export const getDeletionProgress = state => fromAlbum.getDeletionProgress(state.album);
export const getImgGroups = (state) => fromAlbum.getImgGroups(state.album);
export const getImgGroupById = (state, id) => fromAlbum.getImgGroupById(state.album, id);
export const getImgGroupIds = (state) => fromAlbum.getImgGroupIds(state.album);
export const getImgGroupsExport = (state) => fromAlbum.getImgGroupsExport(state.album);

// albums selectors
export const getAlbums = (state) => fromAlbums.getAlbums(state.albums);
export const getAlbumsStatus = (state) => fromAlbums.getAlbumsStatus(state.albums);

// notifications selectors
export const getNoticications = (state) => fromNotifications.getNoticications(state.notifications);
