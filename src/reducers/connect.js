import {
  CONNECT_REQUEST,
  CONNECT_SUCCESS,
  CONNECT_FAIL,
} from 'actions/connect';

const initState = {
  accessToken: null,
};

export default function connect(state = initState, action) {
  switch (action.type) {

    case CONNECT_REQUEST:
      return {
        accessToken: action.accessToken,
      };

    case CONNECT_SUCCESS:
      return {
        ...state,
        connected: true,
        account: action.account,
      };

    case CONNECT_FAIL:
      return {
        ...state,
        connected: false,
      };

    default:
      return state;

  }
}

export const isConnected = (state) => state.connected;
export const getAccessToken = (state) => state.accessToken;
export const getAccountInfo = (state) => state.account;
