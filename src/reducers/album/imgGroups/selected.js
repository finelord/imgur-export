import {
  REQUEST_ALBUM,
} from 'actions/album';

import {
  SELECT_IMG_GROUPS,
  TOGGLE_IMG_GROUP_SELECTION,
} from 'actions/image';

export default function selected(state = [], action) {
  switch (action.type) {

    case REQUEST_ALBUM:
      return [];

    case SELECT_IMG_GROUPS:
      return [...action.groupIds];

    case TOGGLE_IMG_GROUP_SELECTION: {
      const selectedSet = new Set(state);
      if (selectedSet.has(action.groupId)) {
        selectedSet.delete(action.groupId);
      } else {
        selectedSet.add(action.groupId);
      }
      return [...selectedSet];
    }

    default:
      return state;

  }
}

export const getSelectedImgGroupIds = (state) => state || [];
