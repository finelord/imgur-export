import {
  REQUEST_ALBUM_SUCCESS,
} from 'actions/album';

import {
  SET_IMG_GROUP_TITLE_REQUEST,
  SET_IMG_GROUP_TITLE_SUCCESS,
  SET_IMG_GROUP_TITLE_FAIL,
} from 'actions/image';

import { buildImgGroupsById } from 'utils/albumUtils';

export default function byId(state = {}, action) {
  switch (action.type) {

    case REQUEST_ALBUM_SUCCESS:
      return buildImgGroupsById(action.album.images);

    case SET_IMG_GROUP_TITLE_REQUEST:
      return {
        ...state,
        [action.groupId]: {
          ...state[action.groupId],
          loading: true,
        },
      };

    case SET_IMG_GROUP_TITLE_SUCCESS:
      return {
        ...state,
        [action.groupId]: {
          ...state[action.groupId],
          title: action.groupTitle,
          loading: false,
        },
      };

    case SET_IMG_GROUP_TITLE_FAIL:
      return {
        ...state,
        [action.groupId]: {
          ...state[action.groupId],
          loading: false,
        },
      };

    default:
      return state;

  }
}

export const getImgGroupById = (state, id) => state[id] || [];
export const getImgGroupIds = (state) => Object.keys(state).sort() || [];
