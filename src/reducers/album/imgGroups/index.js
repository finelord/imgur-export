import { combineReducers } from 'redux';
import { buildExportImgGroup } from 'utils/albumUtils';
import byId, * as fromById from './byId';
import selected, * as fromSelected from './selected';
import sortOrder, * as fromSortOrder from './sortOrder';

export default combineReducers({
  byId,
  selected,
  sortOrder,
});

export const getImgGroupById = (state, id) => fromById.getImgGroupById(state.byId, id);

export const getImgGroupIds = (state) => {
  let sorted = fromSortOrder.getSortedImgGroupIds(state.sortOrder);
  const unsorted = fromById.getImgGroupIds(state.byId);
  // if some images were deleted
  sorted.forEach(id => {
    if (!unsorted.includes(id)) {
      const index = sorted.indexOf(id);
      sorted.splice(index, 1);
    }
  });
  // if some images were added
  unsorted.forEach(id => {
    if (!sorted.includes(id)) {
      sorted.push(id);
    }
  });
  return [...sorted];
}

export const getSelectedImgGroupIds = (state) =>
  fromSelected.getSelectedImgGroupIds(state.selected);

export const getImgGroups = (state) => {
  const selectedSet = new Set(getSelectedImgGroupIds(state));
  return getImgGroupIds(state).map((id) => ({
    ...state.byId[id],
    selected: selectedSet.has(id),
  })) || [];
};

export const getImgGroupsExport = (state) => {
  const selected = getSelectedImgGroupIds(state);
  return getImgGroupIds(state)
    .filter(id => selected.indexOf(id) > -1)
    .map(id => buildExportImgGroup(getImgGroupById(state, id))) || [];
};
