import {
  SET_IMG_GROUPS_SORT_ORDER_REQUEST,
  SET_IMG_GROUPS_SORT_ORDER_FAIL,
} from 'actions/image';

import {
  REQUEST_ALBUM_SUCCESS,
} from 'actions/album';

export default function selected(state = [], action) {
  switch (action.type) {

    case SET_IMG_GROUPS_SORT_ORDER_REQUEST:
    case SET_IMG_GROUPS_SORT_ORDER_FAIL:
      return [...action.sortOrder];

    case REQUEST_ALBUM_SUCCESS: {
      // TODO: check if localStorage is available
      const descriptionsById = JSON.parse(localStorage.getItem('descriptionsById')) || {};
      const localDescription = descriptionsById[action.album.id] || {};
      const localGeneration = localDescription.generation || 0;

      const remoteDescription = JSON.parse(action.album.description || '{}');
      const remoteGeneration = remoteDescription.generation || 0;

      const sortOrder = localGeneration >= remoteGeneration ?
        localDescription.sortOrder || [] :
        remoteDescription.sortOrder || [];

      return sortOrder;
    }

    default:
      return state;

  }
}

export const getSortedImgGroupIds = (state) => state || [];
