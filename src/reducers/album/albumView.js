import {
  SET_ALBUM_VIEW,
} from 'actions/albumView';

const initState = 'GRID_GROUPED';

export default function albumView(state = initState, action) {
  switch (action.type) {

    case SET_ALBUM_VIEW:
      return action.view;

    default:
      return state;

  }
}

export const getAlbumView = (state) => state;
