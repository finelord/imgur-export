import {
  REQUEST_ALBUM_SUCCESS,
} from 'actions/album';

export default function byId(state = {}, action) {
  switch (action.type) {

    case REQUEST_ALBUM_SUCCESS: {
      const images = {};
      action.album.images.forEach(image => { images[image.id] = image; });
      return images;
    }

    default:
      return state;

  }
}

export const getImageById = (state, id) => state[id] || {};
