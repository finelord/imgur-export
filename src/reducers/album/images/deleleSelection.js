import {
  DELETE_SELECTED_IMAGES_REQUEST,
  DELETE_IMAGE_SUCCESS,
  DELETE_IMAGE_FAIL,
  DELETE_SELECTED_IMAGES_SUCCESS,
  DELETE_SELECTED_IMAGES_FAIL,
} from 'actions/image';

const initState = {
  inProgress: false,
  error: false,
  imageIds: [],
  deleted: [],
  failed: [],
};

export default function deleteSelection(state = initState, action) {
  switch (action.type) {

    case DELETE_SELECTED_IMAGES_REQUEST: {
      return {
        inProgress: true,
        imageIds: action.payload.selectedImgIds,
        deleted: [],
        failed: [],
      };
    }

    case DELETE_IMAGE_SUCCESS: {
      return {
        ...state,
        deleted: [...state.deleted, action.payload.imageId],
      };
    }

    case DELETE_IMAGE_FAIL: {
      return {
        ...state,
        failed: [...state.deleted, action.payload.imageId],
      };
    }

    case DELETE_SELECTED_IMAGES_SUCCESS: {
      return {
        ...state,
        inProgress: false,
      };
    }

    case DELETE_SELECTED_IMAGES_FAIL: {
      return {
        ...state,
        inProgress: false,
        error: true,
      };
    }

    default: return state;
  }
}

export const getIsImageDeletion = state => state.inProgress;
export const getDeletionProgress = state =>
  `${state.deleted.length} of ${state.imageIds.length} deleted (${state.failed.length} errors)`;
