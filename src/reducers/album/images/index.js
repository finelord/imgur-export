import { combineReducers } from 'redux';
import byId, * as fromById from './byId';
import deleteSelection, * as fromDeleteSelection from './deleleSelection';

export default combineReducers({
  byId,
  deleteSelection,
});

export const getImageById = (state, id) => fromById.getImageById(state.byId, id);

export const getIsImageDeletion = state =>
  fromDeleteSelection.getIsImageDeletion(state.deleteSelection);
export const getDeletionProgress = state =>
  fromDeleteSelection.getDeletionProgress(state.deleteSelection);
