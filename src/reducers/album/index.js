import { combineReducers } from 'redux';
import response, * as fromResponse from './response';
import albumView, * as fromAlbumView from './albumView';
import images, * as fromImages from './images';
import imgGroups, * as fromImgGroups from './imgGroups';

export default combineReducers({
  response,
  albumView,
  images,
  imgGroups,
});

export const getAlbum = (state) => fromResponse.getAlbum(state.response);
export const getImages = (state) => fromResponse.getImages(state.response);

export const getAlbumView = (state) => fromAlbumView.getAlbumView(state.albumView);

export const getImageById = (state, id) => fromImages.getImageById(state.images, id);
export const getIsImageDeletion = state => fromImages.getIsImageDeletion(state.images);
export const getDeletionProgress = state => fromImages.getDeletionProgress(state.images);

export const getImgGroups = (state) => fromImgGroups.getImgGroups(state.imgGroups);
export const getImgGroupById = (state, id) => fromImgGroups.getImgGroupById(state.imgGroups, id);
export const getImgGroupIds = (state) => fromImgGroups.getImgGroupIds(state.imgGroups);
export const getImgGroupsExport = (state) => fromImgGroups.getImgGroupsExport(state.imgGroups);
