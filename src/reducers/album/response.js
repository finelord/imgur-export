import {
  REQUEST_ALBUM,
  REQUEST_ALBUM_SUCCESS,
  REQUEST_ALBUM_FAIL,
} from 'actions/album';

export default function response(state = {}, action) {
  switch (action.type) {

    case REQUEST_ALBUM:
      return {
        ...action.albumMetadata,
        status: 'loading',
      };

    case REQUEST_ALBUM_SUCCESS:
      return {
        ...action.album,
        status: 'success',
      };

    case REQUEST_ALBUM_FAIL:
      return { status: 'failed' };

    default:
      return state;

  }
}

export const getAlbum = (state) => state || {};
export const getImages = (state) => state.images || [];
