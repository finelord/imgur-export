function parseImageName(name) {
  const nameArray = name.split('_');
  const getId = () => nameArray[0] || undefined;
  const getKind = () => nameArray[1] || undefined;
  return { getId, getKind };
}

function isBetterPreview(prev, curr, target, prop) {
  const prevValue = prop ? prev[prop] : prev;
  const currValue = prop ? curr[prop] : curr;
  return (
    prevValue >= currValue &&
    prevValue >= target &&
    currValue >= target
  ) || (
    prevValue < currValue &&
    prevValue < target
  );
}

export function buildImgGroupsById(images) {
  const groupIds = new Set();
  const byId = {};
  images.forEach(originalImage => {
    const image = {
      ...originalImage,
      kind: parseImageName(originalImage.name).getKind(),
    };
    const groupId = parseImageName(image.name).getId();
    if (groupIds.has(groupId)) {
      byId[groupId].images = [...byId[groupId].images, image];
      if (isBetterPreview(byId[groupId].previews['1x'], image, 150, 'height')) {
        byId[groupId].previews['1x'] = image;
      }
      if (isBetterPreview(byId[groupId].previews['2x'], image, 300, 'height')) {
        byId[groupId].previews['2x'] = image;
      }
    } else {
      groupIds.add(groupId);
      byId[groupId] = {
        id: groupId,
        title: image.title,
        images: [image],
        previews: {
          '1x': image,
          '2x': image,
        },
      };
    }
  });
  return byId;
}

const buildExportImage = (image) => ({
  src: image.link,
  w: image.width,
  h: image.height,
});

export function buildExportImgGroup(imgGroup) {
  const images = {};
  imgGroup.images.forEach(
    image => { images[image.kind] = buildExportImage(image); }
  );
  const previews = {};
  Object.keys(imgGroup.previews).forEach(imgKey => {
    const preview = imgGroup.previews[imgKey];
    previews[imgKey] = buildExportImage(preview);
  });
  return {
    title: imgGroup.title,
    images,
    previews,
  };
}
