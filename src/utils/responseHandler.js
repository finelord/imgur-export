import { push } from 'react-router-redux';

export function handleResponse(response, dispatch) {
  if (response.ok) {
    return response.json();
  }
  if (response.status !== 403) dispatch(push('/error'));
  return {};
}
