import { connect } from 'react-redux';
import { getOauthUrl } from 'api/connect';
import { isConnected } from 'reducers';
import { tryConnect } from 'actions/connect';
import Connect from 'components/Connect';

const getClientId = () => localStorage.getItem('clientId');

const mapStateToProps = (state) => ({
  isConnected: isConnected(state),
  clientId: getClientId() || '',
});

const mapDispatchToProps = (dispatch) => ({
  tryConnect: () => dispatch(tryConnect()),
  connect: (clientId) => {
    localStorage.setItem('clientId', clientId);
    location.href = getOauthUrl(clientId);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Connect);
