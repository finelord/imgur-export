import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { fetchAlbums as fetchAlbumsAction } from 'actions/albums';
import { isConnected as getIsConnected } from 'reducers';
import AlbumsManager from 'components/AlbumsManager';

class AlbumsManagerPage extends Component {
  static propTypes = {
    isConnected: PropTypes.bool,
    goToConnectPage: PropTypes.func.isRequired,
    fetchAlbums: PropTypes.func.isRequired,
  }
  componentWillMount() {
    const { isConnected, goToConnectPage, fetchAlbums } = this.props;
    if (!isConnected) {
      goToConnectPage();
    } else {
      fetchAlbums();
    }
  }
  render() {
    const { isConnected } = this.props;
    if (isConnected) return <AlbumsManager />;
    return null;
  }
}

const mapStateToProps = (state) => ({
  isConnected: getIsConnected(state),
});

const mapDispatchToProps = (dispatch) => ({
  goToConnectPage: () => dispatch(push('/connect')),
  fetchAlbums: () => dispatch(fetchAlbumsAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsManagerPage);
