import { connect } from 'react-redux';
import { setImgGroupsSortOrder } from 'actions/image';
// import { notifySuccess, notifyError } from 'actions/notifications';
import GroupedImgSort from 'components/GroupedImgSort';

const mapDispatchToProps = (dispatch) => ({
  setImgGroupsSortOrder: (oldGroupsOrder, newGroupsOrder) => 
    dispatch(setImgGroupsSortOrder(oldGroupsOrder, newGroupsOrder)),
  // copyLink: (link) => clipboard.copy(link).then(
  //   () => dispatch(notifySuccess('Link copied to clipboard')),
  //   () => dispatch(notifyError('Your browser doesn\'t support clipboard API'))
  // ),
});

export default connect(null, mapDispatchToProps)(GroupedImgSort);
