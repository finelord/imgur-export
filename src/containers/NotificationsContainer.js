import { connect } from 'react-redux';
import { getNoticications } from 'reducers';
import Notifications from 'components/Notifications';

const mapStateToProps = (state) => ({
  notifications: getNoticications(state),
});

export default connect(mapStateToProps)(Notifications);
