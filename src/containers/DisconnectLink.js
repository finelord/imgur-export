import { connect } from 'react-redux';
import { disconnect } from 'actions/connect';
import Link from 'components/Link';

const mapDispatchToProps = (dispatch) => ({
  onClick: () => dispatch(disconnect()),
});

export default connect(null, mapDispatchToProps)(Link);
