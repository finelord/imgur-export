import { connect } from 'react-redux';
import clipboard from 'clipboard-polyfill';
import { setImgGroupTitle } from 'actions/image';
import { notifySuccess, notifyError } from 'actions/notifications';
import ImgGroup from 'components/ImgGroup';

const mapDispatchToProps = (dispatch) => ({
  onTitleChange: (seq, title) => dispatch(setImgGroupTitle(seq, title)),
  copyLink: (link) => clipboard.writeText(link).then(
    () => dispatch(notifySuccess('Link copied to clipboard')),
    () => dispatch(notifyError('Your browser doesn\'t support clipboard API'))
  ),
});

export default connect(null, mapDispatchToProps)(ImgGroup);
