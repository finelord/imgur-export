import { connect } from 'react-redux';
import Link from 'components/Link';
import { getAlbumView } from 'reducers';
import { setAlbumView } from 'actions/albumView';

const mapStateToProps = (state, ownProps) => ({
  active: ownProps.view === getAlbumView(state),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setAlbumView(ownProps.view)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
