import { connect } from 'react-redux';
import { getImgGroups } from 'reducers';
import GroupedImagesView from 'components/GroupedImagesView';

const mapStateToProps = (state) => ({
  imgGroups: getImgGroups(state),
});

export default connect(mapStateToProps)(GroupedImagesView);
