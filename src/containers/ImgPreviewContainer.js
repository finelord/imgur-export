import { connect } from 'react-redux';
import { toggleImgGroupSelection } from 'actions/image';
import ImgPreview from 'components/ImgPreview';

const mapDispatchToProps = (dispatch) => ({
  onImageClick: (seq) => dispatch(toggleImgGroupSelection(seq)),
});

export default connect(null, mapDispatchToProps)(ImgPreview);
