import { connect } from 'react-redux';
import { isConnected, getAccountInfo } from 'reducers';
import ConnectStatus from 'components/ConnectStatus';

const mapStateToProps = (state) => ({
  isConnected: isConnected(state),
  accountInfo: getAccountInfo(state),
});

export default connect(mapStateToProps)(ConnectStatus);
