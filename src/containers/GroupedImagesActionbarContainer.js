import { connect } from 'react-redux';
import clipboard from 'clipboard-polyfill';
import {
  selectAllImgGroups,
  deselectAllImgGroups,
  deleteSelectedImages,
} from 'actions/image';
import { notifySuccess, notifyError } from 'actions/notifications';
import { getImgGroups, getImgGroupsExport } from 'reducers';
import GroupedImagesActionbar from 'components/GroupedImagesActionbar';

const copyAsJson = (obj) => clipboard.writeText(JSON.stringify(obj, null, 2));

const mapStateToProps = (state) => ({
  imgGroups: getImgGroups(state),
  imgGroupsExport: getImgGroupsExport(state),
});

const mapDispatchToProps = (dispatch) => ({
  selectAllImgGroups: () => dispatch(selectAllImgGroups()),
  deselectAllImgGroups: () => dispatch(deselectAllImgGroups()),
  copyImgGroupsExportJson: (imgGroupsExport) => {
    copyAsJson(imgGroupsExport).then(
      () => dispatch(notifySuccess('JSON copied to clipboard')),
      () => dispatch(notifyError('Your browser doesn\'t support clipboard API'))
    );
  },
  deleteSelectedImages: (imgGroups) => {
    const selectedImgIds = [];
    imgGroups.filter(imgGroup => imgGroup.selected).forEach(imgGroup => {
      imgGroup.images.forEach(image => selectedImgIds.push(image.id));
    });
    dispatch(deleteSelectedImages(selectedImgIds));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(GroupedImagesActionbar);
