import { connect } from 'react-redux';
import {
  getAlbum,
  getImages,
  getImgGroupIds,
  getAlbumView,
  getIsImageDeletion,
  getDeletionProgress,
} from 'reducers';
import AlbumViewer from 'components/AlbumViewer';

const mapStateToProps = (state) => ({
  status: getAlbum(state).status || '',
  albumTitle: getAlbum(state).title || '',
  imgCount: getImages(state).length || 0,
  imgGroupsCount: getImgGroupIds(state).length || 0,
  albumView: getAlbumView(state),
  isImageDeletion: getIsImageDeletion(state),
  deletionProgress: getDeletionProgress(state),
});

export default connect(mapStateToProps)(AlbumViewer);
