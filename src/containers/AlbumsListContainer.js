import { connect } from 'react-redux';
import { fetchAlbum } from 'actions/album';
import { getAlbum, getAlbums, getAlbumsStatus } from 'reducers';
import AlbumsList from 'components/AlbumsList';

const mapStateToProps = (state) => ({
  albums: getAlbums(state) || [],
  albumsStatus: getAlbumsStatus(state) || '',
  activeAlbum: getAlbum(state) || {},
});

const mapDispatchToProps = (dispatch) => ({
  fetchAlbum: (albumMetadata) => dispatch(fetchAlbum(albumMetadata)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsList);
