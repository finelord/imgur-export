import fetch from 'isomorphic-fetch';

export function updateImageTitle(accessToken, imgId, imgTitle) {
  return fetch(`https://api.imgur.com/3/image/${imgId}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ title: imgTitle }),
  });
}

let requestCnt = 0;
function preventOverload(request) {
  return new Promise(resolve => {
    if (requestCnt >= 3) {
      setTimeout(function () {
        preventOverload(request).then(data => resolve(data));
      }, 1000);
    } else {
      requestCnt++;
      request().then(data => {
        requestCnt--;
        resolve(data);
      });
    }
  });
}

export function deleteImage(accessToken, imgId) {
  function deleteRequest() {
    return fetch(`https://api.imgur.com/3/image/${imgId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
      },
    });
  }
  return preventOverload(deleteRequest);
}
