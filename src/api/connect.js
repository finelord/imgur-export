import fetch from 'isomorphic-fetch';

export function getOauthUrl(clientId) {
  return 'https://api.imgur.com/oauth2/authorize' +
         `?client_id=${clientId}` +
         '&response_type=token';
}

export function fetchAccount(accessToken) {
  return fetch('https://api.imgur.com/3/account/me', {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      Accept: 'application/json',
    },
  });
}
