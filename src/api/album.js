import fetch from 'isomorphic-fetch';

export function fetchAlbum(accessToken, albumId) {
  return fetch(`https://api.imgur.com/3/account/me/album/${albumId}`, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      Accept: 'application/json',
    },
  });
}

export function updateAlbum(accessToken, album) {
  return fetch(`https://api.imgur.com/3/album/${album.id}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ description: album.description }),
  });
}
