import fetch from 'isomorphic-fetch';

export function fetchAlbums(accessToken) {
  return fetch('https://api.imgur.com/3/account/me/albums', {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      Accept: 'application/json',
    },
  });
}
