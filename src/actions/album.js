import * as albumApi from 'api/album';
import { selectAllImgGroups } from './image';
import { notifyError } from './notifications';
import { getAccessToken } from 'reducers';
import { handleResponse } from 'utils/responseHandler';

export const REQUEST_ALBUM = 'REQUEST_ALBUM';
export const REQUEST_ALBUM_SUCCESS = 'REQUEST_ALBUM_SUCCESS';
export const REQUEST_ALBUM_FAIL = 'REQUEST_ALBUM_FAIL';

function requestAlbum(albumMetadata) {
  return {
    type: REQUEST_ALBUM,
    albumMetadata,
  };
}

function requestAlbumSuccess(album) {
  return {
    type: REQUEST_ALBUM_SUCCESS,
    album,
  };
}

function requestAlbumFail() {
  return {
    type: REQUEST_ALBUM_FAIL,
  };
}

export function fetchAlbum(albumMetadata) {
  return (dispatch, getState) => {
    const state = getState();

    dispatch(requestAlbum(albumMetadata));

    albumApi
      .fetchAlbum(getAccessToken(state), albumMetadata.id)
      .then(response => handleResponse(response, dispatch))
      .then(json => {
        if (json.success) {
          dispatch(requestAlbumSuccess(json.data));
          dispatch(selectAllImgGroups());
        } else {
          dispatch(requestAlbumFail());
          dispatch(notifyError('Something went wrong...'));
        }
      });
  };
}
