import * as connectApi from 'api/connect';
import { handleResponse } from 'utils/responseHandler';
import { push } from 'react-router-redux';

export const CONNECT_REQUEST = 'CONNECT_REQUEST';
export const CONNECT_SUCCESS = 'CONNECT_SUCCESS';
export const CONNECT_FAIL = 'CONNECT_FAIL';

function extractAccessTokenFromHash(hash) {
  const match = hash.match(/access_token=(\w+)/);
  return !!match && match[1];
}

function extractAccessToken() {
  const hash = window.location.hash;
  const storageAccessToken = localStorage.getItem('accessToken');
  const hashAccessToken = extractAccessTokenFromHash(hash);
  const accessToken = hashAccessToken || storageAccessToken;
  return accessToken;
}

function saveAccessToken(accessToken) {
  localStorage.setItem('accessToken', accessToken);
}

function removeAccessToken() {
  localStorage.removeItem('accessToken');
}

function connectRequest(accessToken) {
  return {
    type: CONNECT_REQUEST,
    accessToken,
  };
}

function connectSuccess(account) {
  return {
    type: CONNECT_SUCCESS,
    account,
  };
}

function connectFail() {
  return {
    type: CONNECT_FAIL,
  };
}

export function tryConnect() {
  return (dispatch) => {
    const accessToken = extractAccessToken();

    dispatch(connectRequest(accessToken));

    connectApi
      .fetchAccount(accessToken)
      .then(response => handleResponse(response, dispatch))
      .then(json => {
        if (json.success) {
          saveAccessToken(accessToken);
          dispatch(connectSuccess(json.data));
          dispatch(push('/'));
        } else {
          removeAccessToken();
          dispatch(connectFail());
        }
      });
  };
}

export function disconnect() {
  return (dispatch) => {
    removeAccessToken();
    dispatch(push('/connect'));
  };
}
