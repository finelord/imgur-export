export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

function addNotification(notificationId, message, messageType) {
  return {
    type: ADD_NOTIFICATION,
    notificationId,
    message,
    messageType,
  };
}

function removeNotification(notificationId) {
  return {
    type: REMOVE_NOTIFICATION,
    notificationId,
  };
}

export function notify(message, messageType = '') {
  return (dispatch) => {
    const notificationId = Date.now();
    dispatch(addNotification(notificationId, message, messageType));
    setTimeout(() => dispatch(removeNotification(notificationId)), 3000);
  };
}

export const notifySuccess = (message) => notify(message, 'success');
export const notifyError = (message) => notify(message, 'error');
