export const SET_ALBUM_VIEW = 'SET_ALBUM_VIEW';

export function setAlbumView(view) {
  return {
    type: SET_ALBUM_VIEW,
    view,
  };
}
