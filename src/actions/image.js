import * as imageApi from 'api/image';
import * as albumApi from 'api/album';
import { fetchAlbum } from './album';
import {
  getAccessToken,
  getImgGroupById,
  getImgGroupIds,
  getAlbum,
} from 'reducers';
import { notifyError, notifySuccess } from './notifications';
import { handleResponse } from 'utils/responseHandler';

export const SELECT_IMG_GROUPS = 'SELECT_IMG_GROUPS';
export const TOGGLE_IMG_GROUP_SELECTION = 'TOGGLE_IMG_GROUP_SELECTION';

export const SET_IMG_GROUP_TITLE_REQUEST = 'SET_IMG_GROUP_TITLE_REQUEST';
export const SET_IMG_GROUP_TITLE_SUCCESS = 'SET_IMG_GROUP_TITLE_SUCCESS';
export const SET_IMG_GROUP_TITLE_FAIL = 'SET_IMG_GROUP_TITLE_FAIL';

export const DELETE_SELECTED_IMAGES_REQUEST = 'DELETE_SELECTED_IMAGES_REQUEST';
export const DELETE_IMAGE_SUCCESS = 'DELETE_IMAGE_SUCCESS';
export const DELETE_SELECTED_IMAGES_SUCCESS = 'DELETE_SELECTED_IMAGES_SUCCESS';
export const DELETE_IMAGE_FAIL = 'DELETE_IMAGE_FAIL';
export const DELETE_SELECTED_IMAGES_FAIL = 'DELETE_SELECTED_IMAGES_FAIL';

export const SET_IMG_GROUPS_SORT_ORDER_REQUEST = 'SET_IMG_GROUPS_SORT_ORDER_REQUEST';
export const SET_IMG_GROUPS_SORT_ORDER_SUCCESS = 'SET_IMG_GROUPS_SORT_ORDER_SUCCESS';
export const SET_IMG_GROUPS_SORT_ORDER_FAIL = 'SET_IMG_GROUPS_SORT_ORDER_FAIL';

function selectImgGroups(groupIds) {
  return {
    type: SELECT_IMG_GROUPS,
    groupIds,
  };
}

export function selectAllImgGroups() {
  return (dispatch, getState) => {
    const state = getState();
    dispatch(selectImgGroups(getImgGroupIds(state)));
  };
}

export function deselectAllImgGroups() {
  return (dispatch) => dispatch(selectImgGroups([]));
}

export function toggleImgGroupSelection(groupId) {
  return {
    type: TOGGLE_IMG_GROUP_SELECTION,
    groupId,
  };
}

// update image title

function setImgGroupTitleRequest(groupId) {
  return {
    type: SET_IMG_GROUP_TITLE_REQUEST,
    groupId,
  };
}

function setImgGroupTitleSuccess(groupId, groupTitle) {
  return {
    type: SET_IMG_GROUP_TITLE_SUCCESS,
    groupId,
    groupTitle,
  };
}

function setImgGroupTitleFail(groupId) {
  return {
    type: SET_IMG_GROUP_TITLE_FAIL,
    groupId,
  };
}

export function setImgGroupTitle(groupId, groupTitle) {
  return (dispatch, getState) => {
    const state = getState();
    const fakeId = 'fake_id_to_force_an_error';
    const accessToken = getAccessToken(state);
    const imgGroup = getImgGroupById(state, groupId);
    const images = imgGroup.images || [{ id: fakeId }];

    dispatch(setImgGroupTitleRequest(groupId));

    const titleUpdatePromises = images.map(image =>
      imageApi
        .updateImageTitle(accessToken, image.id || fakeId, groupTitle)
        .then(response => handleResponse(response, dispatch))
    );

    Promise.all(titleUpdatePromises).then(result => {
      const success = result.every(el => el.success === true);
      if (success) {
        dispatch(setImgGroupTitleSuccess(groupId, groupTitle));
      } else {
        dispatch(setImgGroupTitleFail(groupId));
      }
    });
  };
}

// delete images

function deleteSelectedImagesRequest(selectedImgIds) {
  return {
    type: DELETE_SELECTED_IMAGES_REQUEST,
    payload: { selectedImgIds },
  };
}

function deleteImageSuccess(imageId) {
  return {
    type: DELETE_IMAGE_SUCCESS,
    payload: { imageId },
  };
}

function deleteImageFail(imageId) {
  return {
    type: DELETE_IMAGE_FAIL,
    payload: { imageId },
  };
}

function deleteSelectedImagesSuccess() {
  return {
    type: DELETE_SELECTED_IMAGES_SUCCESS,
  };
}

function deleteSelectedImagesFail() {
  return {
    type: DELETE_SELECTED_IMAGES_FAIL,
  };
}

export function deleteSelectedImages(selectedImgIds) {
  if (!confirm(`${selectedImgIds.length} images will be deleted`)) return () => {};

  return (dispatch, getState) => {
    const state = getState();
    const fakeId = 'fake_id_to_force_an_error';
    const accessToken = getAccessToken(state);

    dispatch(deleteSelectedImagesRequest(selectedImgIds));

    const deleteImagePromises = selectedImgIds.map(imageId =>
      imageApi
        .deleteImage(accessToken, imageId || fakeId)
        .then(() => dispatch(deleteImageSuccess(imageId)))
        .catch(() => dispatch(deleteImageFail(imageId)))
    );

    Promise.all(deleteImagePromises).then(result => {
      const success = result.every(el => el.type === 'DELETE_IMAGE_SUCCESS');
      const errorsCnt = result.reduce(
        (acc, cur) => cur.type === 'DELETE_IMAGE_SUCCESS' ? acc : acc + 1, 0
      );
      if (success) {
        dispatch(deleteSelectedImagesSuccess());
        dispatch(notifySuccess(`${result.length} images were deleted successfully`));
      } else {
        dispatch(deleteSelectedImagesFail());
        dispatch(notifyError(`${errorsCnt} images were not deleted`));
      }
      const album = getAlbum(state);
      dispatch(fetchAlbum(album));
    });
  };
}

// images sorting

function setImgGroupsSortOrderRequest(newGroupsOrder) {
  return {
    type: SET_IMG_GROUPS_SORT_ORDER_REQUEST,
    sortOrder: newGroupsOrder,
  };
}

function setImgGroupsSortOrderSuccess(newGroupsOrder) {
  return {
    type: SET_IMG_GROUPS_SORT_ORDER_SUCCESS,
    sortOrder: newGroupsOrder,
  };
}

function setImgGroupsSortOrderFail(oldGroupsOrder) {
  return {
    type: SET_IMG_GROUPS_SORT_ORDER_FAIL,
    sortOrder: oldGroupsOrder,
  };
}

export function setImgGroupsSortOrder(oldGroupsOrder, newGroupsOrder) {
  return (dispatch, getState) => {
    dispatch(setImgGroupsSortOrderRequest(newGroupsOrder));

    const state = getState();
    const accessToken = getAccessToken(state);
    const album = getAlbum(state);

    // TODO: check if localStorage is available
    const descriptionsById = JSON.parse(localStorage.getItem('descriptionsById')) || {};
    const localDescription = descriptionsById[album.id] || { generation: 0 };
    const localGeneration = localDescription.generation;

    const remoteDescription = JSON.parse(album.description || '{}');
    const remoteGeneration = remoteDescription.generation || 0;

    const description = {
      ...remoteDescription,
      sortOrder: newGroupsOrder,
      generation: Math.max(localGeneration, remoteGeneration) + 1,
    };
    album.description = JSON.stringify(description);

    albumApi
      .updateAlbum(accessToken, album)
      .then(response => handleResponse(response, dispatch))
      .then(
        () => {
          descriptionsById[album.id] = description;
          localStorage.setItem('descriptionsById', JSON.stringify(descriptionsById));
          dispatch(setImgGroupsSortOrderSuccess(newGroupsOrder));
        },
        () => dispatch(setImgGroupsSortOrderFail(oldGroupsOrder))
      );
  };
}
