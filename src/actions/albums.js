import * as albumsApi from 'api/albums';
import { getAccessToken } from 'reducers';
import { handleResponse } from 'utils/responseHandler';

export const REQUEST_ALBUMS = 'REQUEST_ALBUMS';
export const REQUEST_ALBUMS_SUCCESS = 'REQUEST_ALBUMS_SUCCESS';
export const REQUEST_ALBUMS_FAIL = 'REQUEST_ALBUMS_FAIL';

export function requestAlbums() {
  return {
    type: REQUEST_ALBUMS,
  };
}

export function requestAlbumsSuccess(albumsList) {
  return {
    type: REQUEST_ALBUMS_SUCCESS,
    albumsList,
  };
}

export function requestAlbumsFail() {
  return {
    type: REQUEST_ALBUMS_FAIL,
  };
}

export function fetchAlbums() {
  return (dispatch, getState) => {
    const state = getState();

    dispatch(requestAlbums());

    return albumsApi
      .fetchAlbums(getAccessToken(state))
      .then(response => handleResponse(response, dispatch))
      .then(json => {
        if (json.success) {
          dispatch(requestAlbumsSuccess(json.data));
        } else {
          dispatch(requestAlbumsFail());
        }
      });
  };
}
