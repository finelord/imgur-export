import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import 'semantic-ui-css/components/label.css';
import './ImgPreview.scss';

const ImgPreview = ({
  imgGroup,
  onImageClick,
}) => {
  let imgClassName = classNames({
    'img-preview': true,
    selected: imgGroup.selected,
  });

  return (
    <div
      className={imgClassName}
      id={imgGroup.id}
      onClick={e => onImageClick(e.currentTarget.id)}
    >
      <div className="img-border"></div>
      <div className="ui green mini right corner label"></div>
      <img src={imgGroup.previews['2x'].link} alt="Preview" />
    </div>
  );
};

ImgPreview.propTypes = {
  imgGroup: PropTypes.object.isRequired,
  onImageClick: PropTypes.func.isRequired,
};

export default ImgPreview;
