import React from 'react';
import PropTypes from 'prop-types';

const ConnectStatus = ({ isConnected, accountInfo }) => {
  switch (isConnected) {

    case true: {
      const profileName = accountInfo.url
        ? <strong>{accountInfo.url}</strong>
        : 'undefined source';
      return <span>connected to {profileName}</span>;
    }

    case false:
      return <span>no connection</span>;

    default:
      return null;
  }
};

ConnectStatus.propTypes = {
  isConnected: PropTypes.bool,
  accountInfo: PropTypes.object,
};

export default ConnectStatus;
