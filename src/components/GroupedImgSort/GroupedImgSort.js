import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';
import ImgSortItem from 'components/ImgSortItem';

import './GroupedImgSort.scss';

const SortableItem = SortableElement(ImgSortItem);

const SortableList = SortableContainer(({ imgGroups }) => (
  <div className="sortable-container">
    {
      imgGroups.map((imgGroup, index) =>
        <SortableItem
          key={imgGroup.id}
          index={index}
          imgGroup={imgGroup}
        />
      )
    }
  </div>
));

class GroupedImagesSort extends Component {

  static propTypes = {
    imgGroups: PropTypes.array.isRequired,
    setImgGroupsSortOrder: PropTypes.func.isRequired,
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    if (oldIndex === newIndex) return;
    const { imgGroups, setImgGroupsSortOrder } = this.props;
    const oldSortOrder = imgGroups.map(imgGroup => imgGroup.id);
    const newSortOrder = arrayMove(oldSortOrder, oldIndex, newIndex);
    setImgGroupsSortOrder(oldSortOrder, newSortOrder);
  };

  render() {
    const { imgGroups } = this.props;
    return (
      <SortableList
        imgGroups={imgGroups}
        onSortEnd={this.onSortEnd}
        helperClass="img-sort-item--dragging"
        useWindowAsScrollContainer
      />
    );
  }

}

export default GroupedImagesSort;
