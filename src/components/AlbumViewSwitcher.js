import React from 'react';
import SwitcherLink from 'containers/AlbumViewSwitcherLink';

import 'semantic-ui-css/components/menu.css';

const AlbumViewSwitcher = () => (
  <div className="ui secondary pointing menu">

    <SwitcherLink view="GRID_GROUPED">
      Grouped Grid
    </SwitcherLink>

    <SwitcherLink view="LIST_GROUPED">
      Grouped List
    </SwitcherLink>

    <SwitcherLink view="SORT_GROUPED">
      Sort Groups
    </SwitcherLink>

  </div>
);

export default AlbumViewSwitcher;
