import React from 'react';
import PropTypes from 'prop-types';
import Loader from './Loader';
import Link from './Link';

import 'semantic-ui-css/components/menu.css';

const AlbumsList = ({
  albums,
  albumsStatus,
  activeAlbum,
  fetchAlbum,
}) => (
  <div className="ui secondary vertical fluid pointing menu">
    {(() => {
      switch (albumsStatus) {
        case 'loading': return <Loader />;
        case 'failed': return <div>error loading</div>;
        case 'success':
          return (
            albums.map(album =>
              <Link
                key={album.id}
                active={album.id === activeAlbum.id}
                onClick={() => fetchAlbum(album)}
              >
                {album.title}
              </Link>
            )
          );
        default: return null;
      }
    })()}
  </div>
);

AlbumsList.propTypes = {
  albums: PropTypes.array.isRequired,
  albumsStatus: PropTypes.string.isRequired,
  activeAlbum: PropTypes.object.isRequired,
  fetchAlbum: PropTypes.func.isRequired,
};

export default AlbumsList;
