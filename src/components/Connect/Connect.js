import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';

import 'semantic-ui-css/components/grid.css';
import 'semantic-ui-css/components/header.css';
import 'semantic-ui-css/components/button.css';
import 'semantic-ui-css/components/input.css';
import './Connect.scss';

export default class Connect extends Component {

  static propTypes = {
    isConnected: PropTypes.bool,
    clientId: PropTypes.string.isRequired,
    connect: PropTypes.func.isRequired,
    tryConnect: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { tryConnect } = this.props;
    tryConnect();
  }

  render() {
    const { isConnected, clientId, connect } = this.props;
    return (
      <div className="ui middle aligned center aligned grid connect-page">
        {(() => {
          switch (isConnected) {
            case true: return null;
            case false: return (
              <div className="column">
                <h2 className="ui header">please connect to imgur</h2>
                <div className="ui action input">
                  <input
                    type="text"
                    placeholder="Client ID"
                    defaultValue={clientId}
                    ref="clientIdInput"
                  />
                  <a
                    className="ui blue button"
                    onClick={() => connect(this.refs.clientIdInput.value)}
                  >
                    connect
                  </a>
                </div>
              </div>
            );
            default: return <Loader />;
          }
        })()}
      </div>
    );
  }

}
