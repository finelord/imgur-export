import React from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-css/components/dimmer.css';
import 'semantic-ui-css/components/loader.css';

const Loader = ({ message }) => (
  <div className="ui active inverted dimmer">
    <div className="ui text loader">{message || ''}</div>
  </div>
);

Loader.propTypes = {
  message: PropTypes.string,
};

export default Loader;
