import React from 'react';
import PropTypes from 'prop-types';
import Link from './Link';

import 'semantic-ui-css/components/list.css';

const GroupedImagesActionbar = ({
  selectAllImgGroups,
  deselectAllImgGroups,
  imgGroups,
  imgGroupsExport,
  copyImgGroupsExportJson,
  deleteSelectedImages,
}) => (
  <div className="ui horizontal divided link list">

    <Link onClick={() => selectAllImgGroups()}>
      select all
    </Link>

    <Link onClick={() => deselectAllImgGroups()}>
      deselect all
    </Link>

    <Link onClick={() => copyImgGroupsExportJson(imgGroupsExport)}>
      copy selected as json
    </Link>

    <Link onClick={() => deleteSelectedImages(imgGroups)}>
      delete selected
    </Link>

  </div>
);

GroupedImagesActionbar.propTypes = {
  selectAllImgGroups: PropTypes.func.isRequired,
  deselectAllImgGroups: PropTypes.func.isRequired,
  imgGroups: PropTypes.array.isRequired,
  imgGroupsExport: PropTypes.array.isRequired,
  copyImgGroupsExportJson: PropTypes.func.isRequired,
  deleteSelectedImages: PropTypes.func.isRequired,
};

export default GroupedImagesActionbar;
