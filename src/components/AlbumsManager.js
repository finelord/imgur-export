import React from 'react';
import Navbar from './Navbar';
import AlbumsListContainer from 'containers/AlbumsListContainer';
import AlbumViewerContainer from 'containers/AlbumViewerContainer';
import NotificationsContainer from 'containers/NotificationsContainer';

import 'semantic-ui-css/components/container.css';
import 'semantic-ui-css/components/grid.css';

const AlbumsManager = () => (
  <div className="ui container">
    <Navbar />
    <div className="ui grid">
      <div className="five wide column">
        <AlbumsListContainer />
      </div>
      <div className="eleven wide column">
        <AlbumViewerContainer />
      </div>
    </div>
    <NotificationsContainer />
  </div>
);

export default AlbumsManager;
