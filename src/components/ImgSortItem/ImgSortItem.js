import React from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-css/components/item.css';
import 'semantic-ui-css/components/image.css';
import 'semantic-ui-css/components/header.css';
import './ImgSortItem.scss';

const ImgSortItem = ({
  imgGroup,
}) => (
  <div className="ui items">
    <div className="item img-sort-item" id={imgGroup.id}>
      <div className="ui tiny image">
        <img src={imgGroup.previews['2x'].link} alt="preview" />
      </div>
      <div className="content">
        <h4 className="ui disabled header img-sort-item__id">
          {imgGroup.id}
        </h4>
        <h4 className="ui header">{imgGroup.title}</h4>
      </div>
    </div>
  </div>
);

ImgSortItem.propTypes = {
  imgGroup: PropTypes.object.isRequired,
};

export default ImgSortItem;
