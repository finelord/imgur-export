import React from 'react';
import PropTypes from 'prop-types';
import ImgGroupContainer from 'containers/ImgGroupContainer';

import 'semantic-ui-css/components/item.css';
import './GroupedImgList.scss';

const GroupedImagesList = ({ imgGroups }) => (
  <div className="ui divided items grouped-img-list">
    {
      imgGroups.map(imgGroup =>
        <ImgGroupContainer
          key={imgGroup.id}
          imgGroup={imgGroup}
        />
      )
    }
  </div>
);

GroupedImagesList.propTypes = {
  imgGroups: PropTypes.array.isRequired,
};

export default GroupedImagesList;
