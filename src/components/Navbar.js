import React from 'react';
import Link from './Link';
import ConnectStatusContainer from 'containers/ConnectStatusContainer';
import DisconnectLink from 'containers/DisconnectLink';

import 'semantic-ui-css/components/menu.css';

const Navbar = () => (
  <div className="ui secondary pointing menu">
    <div className="right menu">

      <Link disabled>
        <ConnectStatusContainer />
      </Link>

      <DisconnectLink>
        disconnect
      </DisconnectLink>

    </div>
  </div>
);

export default Navbar;
