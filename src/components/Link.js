import React from 'react';
import PropTypes from 'prop-types';

const Link = ({ children, active, disabled, onClick }) => {
  if (active) {
    return (
      <a className="item active">{children}</a>
    );
  } else if (disabled) {
    return (
      <span className="item">{children}</span>
    );
  }
  return (
    <a className="item" onClick={() => onClick()}>{children}</a>
  );
};

Link.propTypes = {
  children: PropTypes.node.isRequired,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Link;
