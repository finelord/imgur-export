import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ImgPreviewContainer from 'containers/ImgPreviewContainer';

import 'semantic-ui-css/components/image.css';
import 'semantic-ui-css/components/input.css';
import 'semantic-ui-css/components/list.css';

export default class ImgGroup extends Component {

  static propTypes = {
    imgGroup: PropTypes.object.isRequired,
    onTitleChange: PropTypes.func.isRequired,
    copyLink: PropTypes.func.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.imgGroup.selected === this.props.imgGroup.selected &&
        nextProps.imgGroup.loading === this.props.imgGroup.loading) {
      return false;
    }
    return true;
  }

  render() {
    const { imgGroup, onTitleChange, copyLink } = this.props;

    let imgTitleClassName = classNames({
      'ui icon fluid transparent big input': true,
      loading: imgGroup.loading || false,
    });

    return (
      <div className="item">

        <div className="ui medium image">
          <ImgPreviewContainer imgGroup={imgGroup} />
        </div>

        <div className="content">
          <div className={imgTitleClassName}>
            <input
              type="text"
              defaultValue={imgGroup.title}
              placeholder="No Title"
              id={imgGroup.id}
              onBlur={(e) => {
                if (imgGroup.title !== e.currentTarget.value) {
                  onTitleChange(e.currentTarget.id, e.currentTarget.value);
                }
              }}
            />
            <i className="search icon"></i>
          </div>
          <div className="ui small selection list">
            {
              imgGroup.images.map(image =>
                <div
                  key={image.id}
                  className="item"
                  onClick={() => copyLink(image.link)}
                >
                  <div className="content">
                    {image.kind} ({image.width} x {image.height},
                    {Math.round(image.size / 1000)}kb, {image.type})
                  </div>
                </div>
              )
            }
          </div>
        </div>

      </div>
    );
  }

}
