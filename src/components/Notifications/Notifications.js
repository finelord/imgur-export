import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import 'semantic-ui-css/components/message.css';
import './Notifications.scss';

const Notifications = ({
  notifications,
}) => (
  <div className="notifications">
    {
      notifications.map((notification) =>
        <div
          key={notification.notificationId}
          className={classNames('ui tiny message', notification.messageType)}
        >
          {notification.message}
        </div>
      )
    }
  </div>
);

Notifications.propTypes = {
  notifications: PropTypes.array.isRequired,
};

export default Notifications;
