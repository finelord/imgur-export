import React from 'react';
import PropTypes from 'prop-types';
import GroupedImagesActionbarContainer from 'containers/GroupedImagesActionbarContainer';
import GroupedImgGrid from './GroupedImgGrid';
import GroupedImgList from './GroupedImgList';
import GroupedImgSortContainer from 'containers/GroupedImgSortContainer';

import 'semantic-ui-css/components/divider.css';

const GroupedImagesView = ({ imgGroups, layout }) => (
  <div>

    <GroupedImagesActionbarContainer />

    <div className="ui divider"></div>

    {(() => {
      switch (layout) {
        case 'grid': return <GroupedImgGrid imgGroups={imgGroups} />;
        case 'list': return <GroupedImgList imgGroups={imgGroups} />;
        case 'sort': return <GroupedImgSortContainer imgGroups={imgGroups} />;
        default: return null;
      }
    })()}

  </div>
);

GroupedImagesView.propTypes = {
  imgGroups: PropTypes.array.isRequired,
  layout: PropTypes.string.isRequired,
};

export default GroupedImagesView;
