import React, { Component } from 'react';
import PropTypes from 'prop-types';
import flexImages from 'javascript-flex-images';
import ImgPreviewContainer from 'containers/ImgPreviewContainer';

import './GroupedImgGrid.scss';

export default class GroupedImagesGrid extends Component {

  static propTypes = {
    imgGroups: PropTypes.array.isRequired,
  }

  componentDidMount() {
    flexImages({ selector: '.flex-images', rowHeight: 150 });
  }

  render() {
    const { imgGroups } = this.props;
    return (
      <div className="flex-images">
        {
          imgGroups.map(imgGroup => (
            <div
              key={imgGroup.id}
              className="item"
              data-h={imgGroup.previews['2x'].height}
              data-w={imgGroup.previews['2x'].width}
            >
              <ImgPreviewContainer imgGroup={imgGroup} />
            </div>
          ))
        }
      </div>
    );
  }

}
