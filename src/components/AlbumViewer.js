import React from 'react';
import PropTypes from 'prop-types';
import Loader from './Loader';
import AlbumViewSwitcher from './AlbumViewSwitcher';
import GroupedImagesViewContainer from 'containers/GroupedImagesViewContainer';

import 'semantic-ui-css/components/header.css';

const AlbumViewer = ({
  status,
  albumTitle,
  imgCount,
  imgGroupsCount,
  albumView,
  isImageDeletion,
  deletionProgress,
}) => {
  if (!status) {
    return null;
  } else if (isImageDeletion) {
    return <Loader message={deletionProgress} />;
  } else if (status === 'loading') {
    return <Loader />;
  } else if (status === 'success') {
    return (
      <div>
        <h2 className="ui header">
          {albumTitle}
          <div className="sub header">
            Images: {imgCount}, Groups: {imgGroupsCount}
          </div>
        </h2>
        <AlbumViewSwitcher />
        {(() => {
          switch (albumView) {
            case 'GRID_GROUPED':
              return <GroupedImagesViewContainer layout="grid" />;
            case 'LIST_GROUPED':
              return <GroupedImagesViewContainer layout="list" />;
            case 'SORT_GROUPED':
              return <GroupedImagesViewContainer layout="sort" />;
            default:
              return <div>NO VIEW</div>;
          }
        })()}
      </div>
    );
  }
  return <div>ERROR</div>;
};

AlbumViewer.propTypes = {
  status: PropTypes.string.isRequired,
  albumTitle: PropTypes.string.isRequired,
  imgCount: PropTypes.number.isRequired,
  imgGroupsCount: PropTypes.number.isRequired,
  albumView: PropTypes.string.isRequired,
  isImageDeletion: PropTypes.bool.isRequired,
  deletionProgress: PropTypes.string.isRequired,
};

export default AlbumViewer;
