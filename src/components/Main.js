import React, { Component } from 'react';
import PropTypes from 'prop-types';

import 'semantic-ui-css/components/reset.css';
import 'semantic-ui-css/components/site.css';

export default class Main extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired,
  };

  render() {
    return (
      <div>
          {/* this will render the child routes */}
          {this.props.children}
      </div>
    );
  }
}
