import React from 'react';
import { Route } from 'react-router';
import App from 'containers/App';
import * as containers from 'containers';

const {
  AlbumsManagerPage,
  ConnectPage,
  NotFoundPage,
} = containers;

export default (
  <Route component={App}>
    <Route path="/" component={AlbumsManagerPage} />
    <Route path="/connect" component={ConnectPage} />
    <Route path="*" component={NotFoundPage} />
  </Route>
);
